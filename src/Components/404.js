import React, { Component } from 'react';

class NotFound extends Component {

    render() {
        return (
            <div>
                <div className="d-flex">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div>
                                    <h1>Oops!</h1>
                                    <h2>404 Not Found</h2>
                                    <div>
                                        Sorry, Requested Data not found!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NotFound;