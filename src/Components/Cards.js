import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import temp from "../img/temp.png";
import cloud from "../img/cloud.png";
import humidity from "../img/humidity.png";
import location from "../img/location.png";
import pressure from "../img/pressure.png";
import sealevel from "../img/sealevel.png";
import visibility from "../img/visibility.png";
import wind from "../img/wind.png";
import sunrise from "../img/sunrise.png";
import sunset from "../img/sunset.png";
import sky from "../img/sky.png";
import groundlevel from "../img/groundlevel.png";
import moment from 'moment';

class CardComponent extends Component {

    render() {

        return (
            <div className="text-white m-3">
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-4">
                            <Card className="m-2 bg-success">
                                <Card.Header>Location <img className="alignment-right" src={location} alt="location" /></Card.Header>
                                <Card.Body>
                                    Place Name: {this.props.weather.name || "NA"} <hr />
                                    Latitude: {this.props.weather.coord.lat || "NA"} <hr />
                                    Longitude: {this.props.weather.coord.lon || "NA"}
                                </Card.Body>
                            </Card>
                        </div>

                        <div className="col-md-4">
                            <Card className="m-2 bg-danger">
                                <Card.Header>Temperature <img className="alignment-right" src={temp} alt="temp" /></Card.Header>
                                <Card.Body>
                                    Current Temperature :  {(parseFloat(this.props.weather.main.temp) - 273.15).toFixed(2) + "°C" || "NA"} <hr />
                                    Temperature Max :  {(parseFloat(this.props.weather.main.temp_max) - 273.15).toFixed(2) + "°C" || "NA"} <hr />
                                    Temperature Min :  {(parseFloat(this.props.weather.main.temp_min) - 273.15).toFixed(2) + "°C" || "NA"}
                                </Card.Body>
                            </Card>
                        </div>

                        <div className="col-md-4">
                            <Card className="m-2 bg-primary">
                                <Card.Header>Sun </Card.Header>
                                <Card.Body>
                                    Sun Rise :  {moment.unix(this.props.weather.sys.sunrise).format('MMMM Do YYYY, h:mm:ss a') || "NA"} <img className="alignment-right" src={sunrise} alt="Sun Rise" /><hr />
                                    Sun Set  : {moment.unix(this.props.weather.sys.sunset).format('MMMM Do YYYY, h:mm:ss a') || "NA"} <img className="alignment-right" src={sunset} alt="Sun Set" /> <hr />
                                    Sea Level: {this.props.weather.main.sea_level + " hPa" || "NA"}  <img className="alignment-right" src={sealevel} alt="Sea Level" />
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                </div>

                <div className="col-md-12 ">
                    <div className="row">
                        <div className="col-md-4">
                            <Card className="m-2 bg-danger">
                                <Card.Header>Humidity & Pressure  </Card.Header>
                                <Card.Body>
                                    Humidity: {this.props.weather.main.humidity + "%" || "NA"} <img className="alignment-right" src={humidity} alt="Humidity" /> <hr />
                                    Pressure: {this.props.weather.main.pressure + "mb" || "NA"} <img className="alignment-right" src={pressure} alt="Pressure" /> <hr />
                                    Ground Level: {this.props.weather.main.grnd_level || "NA"} <img className="alignment-right" src={groundlevel} alt="Pressure" />
                                </Card.Body>
                            </Card>
                        </div>

                        <div className="col-md-4">
                            <Card className="m-2 bg-primary">
                                <Card.Header>Wind <img className="alignment-right" src={wind} alt="Wind" /></Card.Header>
                                <Card.Body>
                                    Deg : {this.props.weather.wind.deg || "NA"} <hr />
                                    Gust : {this.props.weather.wind.gust + "km/h" || "NA"}<hr />
                                    Speed : {this.props.weather.wind.speed + "km/h" || "NA"}
                                </Card.Body>
                            </Card>
                        </div>

                        <div className="col-md-4">
                            <Card className="m-2 bg-success">
                                <Card.Header>Clouds & Visibility </Card.Header>
                                <Card.Body>
                                    Clouds: {this.props.weather.clouds.all || "NA"} <img className="alignment-right" src={cloud} alt="Clouds" /><hr />
                                    Visibility: {this.props.weather.visibility || "NA"} <img className="alignment-right" src={visibility} alt="Visibility" /><hr />
                                    Sky : {this.props.weather.weather.map((key, id) => (<span key={id}>{(key.description)}</span>)) || "NA"} <img className="alignment-right" src={sky} alt="Sky" />
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CardComponent;