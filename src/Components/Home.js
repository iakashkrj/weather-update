import React, { Component } from 'react';
import { Navbar, Container, Nav, Form, FormControl, Button } from "react-bootstrap";
import axios from 'axios';
import CardComponent from "./Cards";
import NotFound from "./404";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            status: "",
            weather: {}
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(e) {
        e.preventDefault();
        this.setState({
            search: e.target.value
        });

    }

    handleClick(e) {
        e.preventDefault();
        let search = this.state.search;
        axios.post(`https://api.openweathermap.org/data/2.5/weather?zip=`+search+`,in&appid=7f6edfd05c381e6d546e42cc24c0bcbd`)
        .then(res => {
          this.setState({
            status: res.request.status,
            weather: res.data
        })
        }).catch(error => {
            this.setState({
                status: 404
            })
          });
    }

    render() {
        return (
            <div>
                <Navbar bg="primary" variant="dark">
                    <Container>
                        <Navbar.Brand href="#home">Weather App</Navbar.Brand>
                        <Nav className="me-auto">
                        </Nav>
                        <Form className="d-flex mr-4">
                            <FormControl
                                type="search"
                                placeholder="Enter Your ZIP Code"
                                aria-label="Search"
                                name="search"
                                value={this.state.search}
                                onChange={this.handleInputChange}
                            />
                            <Button type="submit" variant="outline-success" className="bg-warning" onClick={e => this.handleClick(e)}>Search</Button>
                        </Form>
                    </Container>
                </Navbar>
                {this.state.status === 200 ? <CardComponent weather={this.state.weather}/> : ""}
                {this.state.status === 404 ? <NotFound /> : ""}
            </div>
        );
    }
}

export default Home;